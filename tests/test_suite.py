import unittest

import test_nl_query
import test_chatbot_handler
import test_conversion_pipeline_helper_functions
import test_conversion_pipeline_main_functions

loader = unittest.TestLoader()
suite  = unittest.TestSuite()

suite.addTests(loader.loadTestsFromModule(test_nl_query))
suite.addTests(loader.loadTestsFromModule(test_chatbot_handler))
suite.addTests(loader.loadTestsFromModule(test_conversion_pipeline_helper_functions))
suite.addTests(loader.loadTestsFromModule(test_conversion_pipeline_main_functions))

runner = unittest.TextTestRunner()
result = runner.run(suite)