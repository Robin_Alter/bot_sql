# -*- coding: utf-8 -*-
#pylint: skip-file
import sys
import os
sys.path.insert(0,os.path.abspath('../NLtoSQLBot'))

import unittest
from unittest.mock import patch

from chatbot_handler import ChatbotHandler

class TestChatbotHandler(unittest.TestCase):
    def setUp(self):
        self.chatbot_handler = ChatbotHandler()
        
    def test_chatbot_has_activated_assistant(self):
        self.assertIsNotNone(self.chatbot_handler.assistant)
        
    def test_chatbot_has_session(self):
        self.assertIsNotNone(self.chatbot_handler.session)
        
    def test_chatbot_answers_question(self):
        self.assertIsNotNone(self.chatbot_handler.get_chatbot_answer("hallo"))
        
    @patch('chatbot_handler.get_input', return_value='yes')    
    def test_chatbot_answer_when_user_confirms(self, input):
        self.assertTrue(self.chatbot_handler.return_true_if_yes())
        
    @patch('chatbot_handler.get_input', return_value='no')
    def test_chatbot_answer_when_user_declines(self, input):
        self.assertFalse(self.chatbot_handler.return_true_if_yes())
        
if __name__ == '__main__':
    unittest.main()