# -*- coding: utf-8 -*-
#pylint: skip-file
import sys
import os
sys.path.insert(0,os.path.abspath('../NLtoSQLBot'))

import unittest
from unittest.mock import patch

import conversion_pipeline

class TestHelperFunctions(unittest.TestCase):
    def test_find_between_with_2_different_values(self):
        self.assertEqual("Hallo",conversion_pipeline.find_between("1Hallo2Test","1","2"))
    
    def test_find_between_with_more_than_2_values(self):
        self.assertEqual("Hallo",conversion_pipeline.find_between("2/Hallo/Test/äMehr/","/","/"))
        
    def test_find_between_with_less_than_2_values(self):
        self.assertEqual("",conversion_pipeline.find_between("ÄpfelHalloJa","Ä","Ä"))
        
    def test_extract_index_from_input_with_input_being_digit(self):
        self.assertEqual(0,conversion_pipeline.extract_index_from_input("1"))
        
    def test_extract_index_from_input_with_input_being_string(self):
        self.assertEqual(3,conversion_pipeline.extract_index_from_input("The wrong part is 4"))
        
    def test_extract_index_from_input_with_input_containing_no_number(self):
        self.assertRaises(ValueError,conversion_pipeline.extract_index_from_input,"Keine Nummer hier")
        
    def test_extract_index_from_input_with_input_smaller_than_one(self):
        self.assertRaises(ValueError,conversion_pipeline.extract_index_from_input,"0 ist die Zahl")
        
    @patch('conversion_pipeline.get_input', return_value='1')
    def test_get_index_of_wrong_part_with_input_being_digit(self,input):
        self.assertEqual(0,conversion_pipeline.get_index_of_wrong_part())
             
    @patch('conversion_pipeline.get_input', return_value='1 ist eine schöne Nummer')   
    def test_get_index_of_wrong_part_with_input_being_string(self,input):
        self.assertEqual(0,conversion_pipeline.get_index_of_wrong_part())
        
    def test_both_are_valid_relators_when_both_are_valid_relators(self):
        self.assertTrue(conversion_pipeline.both_are_valid_relators("is","is not"))
        
    def test_both_are_valid_relators_when_both_are_not_valid_relators(self):
        self.assertTrue(conversion_pipeline.both_are_valid_relators("Hello","maybe"))
        
    def test_both_are_valid_relators_when_one_is_not_valid_relator(self):
        self.assertFalse(conversion_pipeline.both_are_valid_relators("is","count the"))
    
    def test_both_are_valid_aggregators_when_both_are_valid_aggregators(self):
        self.assertTrue(conversion_pipeline.both_are_valid_aggregators("show the","count the"))
        
    def test_both_are_valid_aggregators_when_both_are_not_valid_aggregators(self):
        self.assertTrue(conversion_pipeline.both_are_valid_aggregators("hello","maybe"))
        
    def test_both_are_valid_aggregators_when_one_is_not_valid_aggregator(self):
        self.assertFalse(conversion_pipeline.both_are_valid_aggregators("count the","maybe"))
        
    def test_more_than_one_part_exists_when_more_than_one_part_exists(self):
        self.assertTrue(conversion_pipeline.more_than_one_part_exists(["hello","How are you"]))
        
    def test_more_than_one_part_exists_when_only_one_part_exists(self):
        self.assertFalse(conversion_pipeline.more_than_one_part_exists(["hello"]))
        
    @patch('conversion_pipeline.get_input', return_value='Die Nummer ist "Hallo" meine Freunde') 
    def test_extract_string_from_input_with_valid_input(self,input):
        self.assertEqual("Hallo",conversion_pipeline.extract_string_from_input())
        
if __name__ == '__main__':
    unittest.main()
    