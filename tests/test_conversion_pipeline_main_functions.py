# -*- coding: utf-8 -*-
#pylint: skip-file
import sys
import os
sys.path.insert(0,os.path.abspath('../NLtoSQLBot'))

import unittest

from conversion_pipeline import ConversionPipeline

def blockPrint():
    sys.stdout = open(os.devnull, 'w')

def enablePrint():
    sys.stdout = sys.__stdout__

class TestMainFunctions(unittest.TestCase):
    def setUp(self):
        self.conversion_pipeline = ConversionPipeline()
        
    def test_get_all_tables_is_not_empty(self):
        self.assertIsNotNone(self.conversion_pipeline.get_all_tables())
        
    def test_map_name_to_id_with_name_existing(self):
        self.assertIsNotNone(self.conversion_pipeline.map_name_to_id("list of european stadiums by capacity".lower().replace(" ","")))
        
    def test_map_name_to_id_with_name_not_existing(self):
        self.assertRaises(NameError,self.conversion_pipeline.map_name_to_id,"WJINFNWOIJ")
        
    def test_set_existing_table_name_with_table_name_existing(self):
        blockPrint()
        self.conversion_pipeline.nl_input = '''the table name is "list of european stadiums by capacity"'''
        
        self.conversion_pipeline.set_existing_table_name()
        self.assertEqual("list of european stadiums by capacity",self.conversion_pipeline.table_input)
        self.assertIsNotNone(self.conversion_pipeline.table_id)
        enablePrint()
        
    def test_set_existing_table_name_with_table_name_not_existing(self):
        blockPrint()
        self.conversion_pipeline.nl_input = '''the table name is "NWIGHWIBF"'''
        
        self.assertRaises(NameError,self.conversion_pipeline.set_existing_table_name)
        self.assertEqual("",self.conversion_pipeline.table_id)
        self.assertEqual("",self.conversion_pipeline.table_input)
        enablePrint()
        
    def test_set_existing_table_name_with_table_already_set_and_new_table_existing(self):
        blockPrint()
        first_input = '''the table name is "list of european stadiums by capacity"'''
        second_input = '''The table name is "Toronto Raptors all-time roster"'''
        self.conversion_pipeline.nl_input = first_input
        self.conversion_pipeline.set_existing_table_name()
        first_id = self.conversion_pipeline.table_id
        first_table_name = self.conversion_pipeline.table_input
        
        self.conversion_pipeline.nl_input = second_input
        self.conversion_pipeline.set_existing_table_name()
        self.assertEqual("Toronto Raptors all-time roster",self.conversion_pipeline.table_input)
        self.assertNotEqual(first_table_name,self.conversion_pipeline.table_input)
        self.assertNotEqual(first_id,self.conversion_pipeline.table_id)
        enablePrint()
        
if __name__ == '__main__':
    unittest.main()    
        