# -*- coding: utf-8 -*-
#pylint: skip-file
import sys
import os
sys.path.insert(0,os.path.abspath('../NLtoSQLBot'))

import unittest
import random

from nl_query import NLQuery
from NLtoSQLConverter.loadTrainData import load_all_data

class TestNlQuery(unittest.TestCase):
    def setUp(self):
        self.model, self.tables = load_all_data()
        table_name = "list of european stadiums by capacity"
        self.table_id = self.tables[self.map_name_to_id(table_name.lower().replace(" ", ""))]
        self.nl_input = "Show all stadiums with a capacity over 40000"
        self.nl_query = NLQuery(self.model,self.table_id,self.nl_input)
        
    def map_name_to_id(self, tname):
        for key, value in self.tables.items():
            if "page_title" in value.keys():
                page_title = value["page_title"].lower()
                if page_title.replace(" ", "") == tname:
                    return key
        
    def test_object_is_created(self):
        self.assertIsNotNone(self.nl_query)
        
    def test_get_suggestions_as_nql(self):
        nql_suggestions = self.nl_query.get_suggestions_as_nql()
        self.assertIsNotNone(nql_suggestions)
        
    def test_get_suggestions_as_sql(self):
        sql_suggestions = self.nl_query.get_suggestions_as_sql()
        self.assertIsNotNone(sql_suggestions)
        
    def test_change_first_suggestion(self):
        old_suggestions = self.nl_query.get_suggestions_as_nql()
        index = 0
        self.nl_query.change_suggestion(index,"Count The Tenants of stadiums in Russia")
        new_suggestions = self.nl_query.get_suggestions_as_nql()
        
        self.assertIsNot(old_suggestions[index], new_suggestions[index])
        for suggestion in old_suggestions:
            if suggestion is not old_suggestions[index]:
                self.assertIn(suggestion, new_suggestions)
                
    def test_change_last_suggestion(self):
        old_suggestions = self.nl_query.get_suggestions_as_nql()
        index = len(old_suggestions)-1
        self.nl_query.change_suggestion(index,"Count The Tenants of stadiums in Russia")
        new_suggestions = self.nl_query.get_suggestions_as_nql()
        
        self.assertIsNot(old_suggestions[index], new_suggestions[index])
        for suggestion in old_suggestions:
            if suggestion is not old_suggestions[index]:
                self.assertIn(suggestion, new_suggestions)
                
    def test_change_random_suggestion(self):
        old_suggestions = self.nl_query.get_suggestions_as_nql()
        index = random.randint(0,len(old_suggestions))
        self.nl_query.change_suggestion(index,"Count The Tenants of stadiums in Russia")
        new_suggestions = self.nl_query.get_suggestions_as_nql()
        
        self.assertIsNot(old_suggestions[index], new_suggestions[index])
        for suggestion in old_suggestions:
            if suggestion is not old_suggestions[index]:
                self.assertIn(suggestion, new_suggestions)
            
if __name__ == '__main__':
    unittest.main()
