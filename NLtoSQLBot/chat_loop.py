# -*- coding: utf-8 -*-
"""ChatLoop

start chatbot, with some introductions.
start loop, which enables to match input to action.

"""
from conversion_pipeline import ConversionPipeline
from print_colors import print_explain, print_info, print_chat_output
from chatbot_handler import get_action, send_generic_answer


class ChatLoop:
    def __init__(self):
        """init ChatLoop

        print an introduction to the chatbot, and starts the chatbot handler and conversion pipeline
        """
        print_introduction()
        self.conversion_pipeline = ConversionPipeline()
        self.chatbot_handler = self.conversion_pipeline.chatbot_handler
        print_info(("Welcome! I am bot designed to convert your Input into SQL queries."
                    "To start let's begin by telling me on which table you want to operate."))
        self.start_main_loop()

    def start_main_loop(self):
        """start main loop

        Starts the main feedback loop for the chatbot. In every step await an input from a user.
        If an input is given get the answer from the chatbot, and then try to match it's intent
        """
        while True:
            nl_input = get_input()
            self.conversion_pipeline.nl_input = nl_input
            message = self.chatbot_handler.get_chatbot_answer(nl_input)
            try:
                self.match_action(get_action(message))
            except BaseException:
                send_generic_answer(message)

    def match_action(self, action):
        """match action

        run code depending, on the registered action, the bot currently supports the 
        following actions.
        convertNQL converts the input into SQL via the conversion pipeline
        setTableName changes the table the bot is working on and
        getAllTables shows all available tables

        Args:
            action: action will be provided by the chatbot Handler
        """
        if action == "convertNQL":
            self.conversion_pipeline.convert_to_nql()
        elif action == "setTableName":
            self.conversion_pipeline.set_table_name()
        elif action == "getAllTables":
            self.conversion_pipeline.print_all_tables()


def print_introduction():
    """print_introduction

    introduction displayed on first start

    """
    print_info("Please wait a moment while I start.")
    print_explain("--------------------------------------------")
    print_info("Color legend:")
    print_explain("--------------------------------------------")
    print_explain("this color will be used for questions I might have")
    print_chat_output("this color will be used for answers I provide")
    print_info("this color will be used for information")
    print_explain("--------------------------------------------")


def get_input():
    """get input

    return input from cli

    """
    return input()
