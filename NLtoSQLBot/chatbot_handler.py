# -*- coding: utf-8 -*-
"""Chatbot Handler

Gets User Input, and tries to match the intent to an action

"""
from ibm_watson import AssistantV2
from ibm_cloud_sdk_core.authenticators import IAMAuthenticator

from print_colors import print_chat_output


class ChatbotHandler:
    def __init__(self):
        """init ChatbotHandler

        Initializes the assistant and starts a session for the chatbot
        """
        self.initialize_assistant()
        self.start_session()

    def initialize_assistant(self):
        """initialize assistant

        Initializes the assistant, by passing the matching authenticator, and then starting the bot
        """
        authenticator = IAMAuthenticator(
            '5eYPvU4eEP6-qPs_nJtuaJ7tX4c8v3WLYPiQ72bYNAgt')
        self.assistant = AssistantV2(
            version='2018-09-20',
            authenticator=authenticator)
        self.assistant.set_service_url(
            'https://gateway-lon.watsonplatform.net/assistant/api')

    def start_session(self):
        """start session

        Starts a session for the bot
        """
        self.session = self.assistant.create_session(
            "34e5fa3a-ae43-4d43-971d-a7f9c4969508").get_result()

    def return_true_if_yes(self):
        """return true if yes

        Waits for a user input, and then tries to tell if it's either a yes or a no.
        If it's neither or the bot can't detect it, it tries again

        Returns:
            bool: True if a positive action gets detected, False if a negative action gets detected
        """
        while True:
            yes_or_no = get_input()
            message = self.get_chatbot_answer(yes_or_no)
            try:
                action = get_action(message)
                if action == "positiveAction":
                    return True
                if action == "negativeAction":
                    return False
            except BaseException:
                send_generic_answer(message)

    def get_chatbot_answer(self, user_input):
        """get chatbot answer

        Gets an answer from the chatbot

        Args:
            user_input (string) : the input, the bot responds to

        Returns:
            message: The answer the chatbot provides
        """
        return self.assistant.message(
            "34e5fa3a-ae43-4d43-971d-a7f9c4969508",
            self.session['session_id'],
            input={'text': user_input},
            context={
                'metadata': {
                    'deployment': 'myDeployment'
                }
            }).get_result()


def send_generic_answer(message):
    """send generic answer

    sends a generic answer, if the bot cannot match the inputs intent

    Args:
        message: the answer of the bot to a message it did not understand
    """
    generic_answer = message["output"]["generic"][0]["text"]
    print_chat_output(generic_answer)


def get_action(message):
    """get action

    extracts the action from a bot response

    Args:
        message: the answer to the user input

    Returns:
        string: the action the bot detected as a String
    """

    return message["output"]["user_defined"]["action"]


def get_input():
    return input()
