# -*- coding: utf-8 -*-
"""output format

beautiful print outputs

"""


def print_explain(stuff):
    """print explain

    green color

    Args:
        stuff: print output string

    """
    print(BColors.OKGREEN + stuff + BColors.ENDC)


def print_chat_output(stuff):
    """print chat output

    blue color

    Args:
        stuff: print output string

    """
    print(BColors.OKBLUE + stuff + BColors.ENDC)


def print_info(stuff):
    """print chat output

    pink color

    Args:
        stuff: print output string

    """
    print(BColors.HEADER + stuff + BColors.ENDC)


class BColors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
