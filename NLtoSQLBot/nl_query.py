# -*- coding: utf-8 -*-
"""NLQuery

this class is for suggestion creation responsible

"""
from NLtoSQLConverter.sqlnet.suggcreator import create_suggestions
from NLtoSQLConverter.models.nql import NQL


class NLQuery:

    def __init__(self, model, current_table, nl_input):
        """init NLQuery

        Args:
            model: working model for converter
            current_table: selected table as string to work on
            nl_input: nl input as string to convert
        """
        self.model = model
        self.nl_input = nl_input
        self.current_table = current_table
        self.create_suggestions()

    def create_suggestions(self):
        """create suggestion

        call create_suggestion from NLtoSQLConverter which return nl objects array. 
        Specific information available on the NLtoSQLConverter master thesis.
        """
        self.nql_objects = []
        suggestions: dict = create_suggestions(
            self.model, self.nl_input, self.current_table)
        for key, value in suggestions.items():
            self.nql_objects.append(NQL.frominl(key))

    def get_suggestions_as_nql(self):
        """get suggestion as nql

        return suggestion as nql
        """
        nql_suggestions = []
        for suggestion in self.nql_objects:
            nql_suggestions.append(suggestion.inl())
        return nql_suggestions

    def get_suggestions_as_sql(self):
        """get suggestion as sql

        return suggestion as sql
        """
        sql_suggestions = []
        for suggestion in self.nql_objects:
            sql_suggestions.append(suggestion.sql())
        return sql_suggestions

    def change_suggestion(self, index, new_suggestion):
        """change suggestion

        change nl query object, return new nl query object.
        """
        try:
            self.nql_objects[index] = NQL.frominl(new_suggestion)
        except BaseException:
            print("The change could not be made")
