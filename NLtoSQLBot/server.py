# -*- coding: utf-8 -*-
"""NQLtoSQL Backend for VSCode

This is our documentation about the interface from chatbot to VSCode plugin.
We have decided to implement the plugin as a client. So the chatbot acts as a simple http server
to answer the plugin's requests.

"""
import argparse
from http.server import HTTPServer
from server_class import ServerClass


def run(
        server_class=HTTPServer,
        handler_class=ServerClass,
        addr="localhost",
        port=8000):
    """run http server

    Args:
        server_class: HTTPServer
        hander_class: S
        addr: listen address
        port: listen port

    """
    server_address = (addr, port)
    httpd = server_class(server_address, handler_class)

    print(f"Starting nqltosql server on {addr}:{port}")
    httpd.serve_forever()


if __name__ == "__main__":
    PARSER = argparse.ArgumentParser(description="Run a simple HTTP server")
    PARSER.add_argument(
        "-l",
        "--listen",
        default="localhost",
        help="Specify the IP address on which the server listens",
    )
    PARSER.add_argument(
        "-p",
        "--port",
        type=int,
        default=8000,
        help="Specify the port on which the server listens",
    )
    ARGS = PARSER.parse_args()
    run(addr=ARGS.listen, port=ARGS.port)
