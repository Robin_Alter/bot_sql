# -*- coding: utf-8 -*-
"""The main file"""
from chat_loop import ChatLoop

if __name__ == '__main__':
    CHAT_LOOP = ChatLoop()
    