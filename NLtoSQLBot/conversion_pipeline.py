# -*- coding: utf-8 -*-
"""Conversion Pipeline

The main pipeline for converting an input into SQL. First generate a suggestion. If the suggestion
is wrong, split it into the select and where parts, and then replace the wrong part with a 
right part

"""
import sys
import os

from print_colors import print_explain, print_info, print_chat_output
from chatbot_handler import ChatbotHandler
from nl_query import NLQuery
from NLtoSQLConverter.loadTrainData import load_all_data

sys.path.insert(0, os.path.abspath('../NLtoSQLBot'))

INDEX_OF_BEST_GUESS = 0
VALID_RELATORS = [
    "is",
    "is higher than",
    "is lower than",
    "is below",
    "is above",
    "is not"]
VALID_AGGREGATORS = [
    "show the",
    "show the highest",
    "show the lowest",
    "show the average",
    "show the sum of all",
    "count the",
    "lowest",
    "highest",
    "average",
    "sum of all"]


class ConversionPipeline:
    def __init__(self):
        self.model, self.tables = load_all_data()
        self.table_id = ""
        self.table_input = ""
        self.nl_input = ""
        self.current_query = NLQuery
        self.chatbot_handler = ChatbotHandler()

    def convert_to_nql(self):
        """convert to nql

        convert to nql use the best guess

        """
        self.try_conversion_to_nql()
        self.print_nql_query(INDEX_OF_BEST_GUESS)
        if self.nql_is_correct():
            self.print_sql_query(INDEX_OF_BEST_GUESS)
        else:
            self.improve_nql()

    def try_conversion_to_nql(self):
        """convert to nql

        try to convert to nql, if not working table is set it will be ask for a table name

        """
        if self.table_id == "":
            print_info(
                ("Before we begin, I need the name of the table you want to work on."
                 "Please put it into quotes."))
        else:
            self.convert_input_to_nql()

    def convert_input_to_nql(self):
        """convert input to nql

        convert input to nql

        """
        self.current_query = NLQuery(self.model, self.table_id, self.nl_input)

    def nql_is_correct(self):
        """nql is correct

        convert input to nql

        """
        print_explain("Is this correct?")
        return self.chatbot_handler.return_true_if_yes()

    def print_nql_query(self, index):
        """convert input to nql

        convert input to nql

        Args:
            index: index from nql suggestion array

        """
        print_chat_output(self.current_query.get_suggestions_as_nql()[index])

    def print_sql_query(self, index):
        """print sql query

        print sql query

        Args:
            index: index from nql suggestion array

        """
        print_info("Great! This is your sql Query:")
        print_chat_output(self.current_query.get_suggestions_as_sql()[
            index].replace("table", self.table_input))

    def improve_nql(self):
        """improve nql

        improve nql
        """
        split_query = self.split_query_into_select_and_where_parts()
        if more_than_one_part_exists(split_query):
            improved_suggestion = self.improve_select_and_where_part(
                split_query)
        else:
            improved_suggestion = self.edit_part(split_query[0])
        self.current_query.change_suggestion(
            INDEX_OF_BEST_GUESS, improved_suggestion)
        self.print_sql_query(INDEX_OF_BEST_GUESS)

    def split_query_into_select_and_where_parts(self):
        """split_query_into_select_and_where_parts

        split nql query object into where and select part.

        Returns:
            List: where and select part

        """
        split_query = []
        nql_objects = self.current_query.nql_objects
        select_part = str(nql_objects[INDEX_OF_BEST_GUESS].selstate.inl())
        where_part = str(
            nql_objects[INDEX_OF_BEST_GUESS].wherestate.inl())
        if select_part:
            split_query.append(select_part)
        if where_part:
            split_query.append(where_part)
        return split_query

    def improve_select_and_where_part(self, split_query):
        """improve_select_and_where_part

        split nql query object into where and select part.

        Returns:
            String: array of where and select which improved strings

        """
        print_info("Ok I split the query into the following parts")
        for count, part in enumerate(split_query):
            print_chat_output(str(count + 1) + ". :" + str(part))

        print_explain("Please tell me which of these Parts is wrong.")
        index_of_wrong_part = get_index_of_wrong_part()
        split_query[index_of_wrong_part] = self.edit_part(
            split_query[index_of_wrong_part])

        print_explain(
            "Great! Was there also something wrong with the other part?")
        if self.chatbot_handler.return_true_if_yes():
            split_query[index_of_wrong_part - \
                1] = self.edit_part(split_query[index_of_wrong_part - 1])

        return split_query[0] + " " + split_query[1]

    def edit_part(self, part):
        """edit_part

        Args:
            part: string

        Returns:
            String: edited part
        """
        print_info("Ok then let's fix it.")
        print_explain(
            """What was wrong with the part: '""" +
            part +
            """'?""")
        error_in_query = extract_string_from_input()

        print_explain("With what should it be replaced?")
        wanted_in_query = extract_string_from_input()

        if not both_are_valid_relators(error_in_query, wanted_in_query):
            wanted_in_query = wait_for_valid_relator()

        if not both_are_valid_aggregators(
                error_in_query, wanted_in_query):
            wanted_in_query = wait_for_valid_aggregator()

        edited_part = part.replace(error_in_query, wanted_in_query)

        print_info(
            "The new part now looks like this: \n" +
            edited_part)
        if self.nql_is_correct():
            return edited_part
        return self.edit_part(edited_part)

    def set_table_name(self):
        """set_table_name

        try to set table name, if table not exists raise error with message (print to cli)
        """
        try:
            self.set_existing_table_name()
        except NameError:
            print_info("The table unfortunately does not exist.")

    def set_existing_table_name(self):
        """set_table_name

        set table name after set_table_name() checked it.

        """
        table_input = find_between(self.nl_input, '"', '"')
        table_name = table_input.lower().replace(" ", "")
        self.table_id = self.tables[self.map_name_to_id(table_name)]
        self.table_input = table_input
        print_info(
            "Alright, I got it. I will work on the following table: " +
            self.table_input)

    def print_all_tables(self):
        """print_all_tables

        print all available tables

        """
        all_tables = self.get_all_tables()
        print_info("These are the tables of the database you are working on: ")
        for table in all_tables:
            print_info(table)

    def map_name_to_id(self, tname):
        """map_name_to_id

        Args:
            tname: Name of table

        Returns:
            String: key of table in tablesdict

        """
        for key, value in self.tables.items():
            if "page_title" in value.keys():
                page_title = value["page_title"].lower()
                if page_title.replace(" ", "") == tname:
                    return key
        raise NameError("There is no Table with the specified name")

    def get_all_tables(self):
        """map_name_to_id

        get name of all tables

        Returns:
            List: tables

        """
        all_tables = []
        for key, value in self.tables.items():
            if "page_title" in value.keys():
                all_tables.append(value["page_title"])
        return all_tables


def extract_string_from_input():
    """extract_string_from_input

    extract string from input (remove quotes)

    """
    while True:
        user_input = get_input()
        part = find_between(user_input, '"', '"')
        if part:
            return part
        print_info("Please put it into quotes!")


def get_input():
    """get_input()

    get input from user (cli)

    """
    return input()


def more_than_one_part_exists(split_query):
    """more_than_one_part_exists

    Args:
        split_query: List of strings (select and where part)

    Returns
        bool: check if more than one available

    """
    return len(split_query) > 1


def both_are_valid_relators(error_in_query, wanted_in_query):
    """both_are_valid_relators

    check if both provided strings are valid relators.

    Args:
        error_in_query (String): error
        wanted_in_query (String):  wanted

    Returns
        bool: true if valid, false if not

    """
    if error_in_query in VALID_RELATORS and wanted_in_query not in VALID_RELATORS:
        return False
    return True


def both_are_valid_aggregators(error_in_query, wanted_in_query):
    """both_are_valid_aggregators

    check if both provided strings are valid aggregators.

    Args:
        error_in_query (String): error
        wanted_in_query (String):  wanted

    Returns
        bool: true if valid, false if not

    """
    if error_in_query in VALID_AGGREGATORS and  \
            wanted_in_query not in VALID_AGGREGATORS:
        return False
    return True


def wait_for_valid_relator():
    """wait_for_valid_relator

    as long as the given relator is not in valid relators, 
    show all valid relators and wait for the user to try again

    Returns
        String: valid relator

    """
    valid_relator = ""
    while valid_relator not in VALID_RELATORS:
        print_info(
            "I could not understand you, please choose one of the following instead: ")
        for relation in VALID_RELATORS:
            print(relation)
        valid_relator = extract_string_from_input()
    return valid_relator


def wait_for_valid_aggregator():
    """wait_for_valid_aggregator

    as long as the given aggregator is not in valid aggregators, 
    show all valid aggregators and wait for the user to try again

    Returns
        String: valid aggregator

    """
    valid_aggregator = ""
    while valid_aggregator not in VALID_AGGREGATORS:
        print_info(
            "I could not understand you, please choose one of the following instead: ")
        for aggregator in VALID_AGGREGATORS:
            print(aggregator)
        valid_aggregator = extract_string_from_input()
    return valid_aggregator


def get_index_of_wrong_part():
    """get_index_of_wrong_part

    get index of wrong part

    Returns
        int: index of wrong part

    """
    while True:
        user_input = get_input()
        try:
            return extract_index_from_input(user_input)
        except ValueError:
            print_info("Please enter a valid nonnegative Number")


def extract_index_from_input(user_input):
    """extract_index_from_input

    check if the input is a digit, if not, check if a digit is in the input. 
    When a digit is detected return the digit, else raise a ValueError

    Args:
        user_input (String): input from user

    Returns
        int: index of input

    """
    if user_input.isdigit() and int(user_input) > 0:
        return int(user_input) - 1
    for index in user_input.split():
        if index.isdigit() and int(index) > 0:
            return int(index) - 1
    raise ValueError


def find_between(input_string, first, last):
    """find_between

    find string between two provided chars

    Args:
        input_string (String): input from user
        first (String): first chars
        last (String): last chars

    Returns
        String: string between first and last from input_string

    """
    try:
        start = input_string.index(first) + len(first)
        end = input_string.index(last, start)
        return input_string[start:end]
    except ValueError:
        return ""
