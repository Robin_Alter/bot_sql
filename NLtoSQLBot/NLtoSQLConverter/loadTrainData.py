#pylint: skip-file

import argparse

import sys
import os
sys.path.append(os.path.join(os.path.abspath('../NLtoSQLBot'),"NLtoSQLConverter"))

import pandas as pd
import json
from collections import OrderedDict
from importlib import reload
from sqlnet.suggcreator import create_suggestions,parse_nql
# os.environ['QT_QPA_PLATFORM_PLUGIN_PATH'] = "D:/Programme/Anaconda3/Library/plugins/platforms"

import torch
from sqlnet.model.sqlnet import SQLNet
from sqlnet.utils import *
from models.nql import NQL

train_emb = False
N_word = 300
B_word = 42

USE_SMALL = False
GPU = False
BATCH_SIZE = 2  # TODO: Back to 64
TEST_ENTRY = (True, True, True)  # (AGG, SEL, COND)

parser = argparse.ArgumentParser()
parser.add_argument('--toy', action='store_true',
                    help='If set, use small data; used for fast debugging.')
parser.add_argument('--ca', action='store_true',default=True,
                    help='Use conditional attention.')
parser.add_argument('--dataset', type=int, default=0,
                    help='0: original dataset, 1: re-split dataset')
parser.add_argument('--rl', action='store_true',
                    help='Use RL for Seq2SQL.')
parser.add_argument('--baseline', action='store_true',
                    help='If set, then test Seq2SQL model; default is SQLNet model.')
parser.add_argument('--train_emb', action='store_true',
                    help='Use trained word embedding for SQLNet.')
args = parser.parse_args()

def createModel():    
    sql_data, table_data, val_sql_data, val_table_data, \
    test_sql_data, test_table_data, \
    TRAIN_DB, DEV_DB, TEST_DB = load_dataset(0, use_small=USE_SMALL)
    
    word_emb = load_word_emb('glove/glove.%dB.%dd.txt' % (B_word, N_word), \
                             load_used=True, use_small=USE_SMALL)  # load_used can speed up loading
    
    model = SQLNet(word_emb, N_word=N_word, use_ca=True, gpu=GPU,
                       trainable_emb=True)
    return model
    
def modelWithTrainEmb(model):
    
    agg_m, sel_m, cond_m, agg_e, sel_e, cond_e = best_model_name(args)
    #print("Loading from %s" % agg_m)
    model.agg_pred.load_state_dict(torch.load(agg_m, map_location = torch.device('cpu')))
    #print("Loading from %s" % sel_m)
    model.sel_pred.load_state_dict(torch.load(sel_m, map_location = torch.device('cpu')))
    #print("Loading from %s" % cond_m)
    model.cond_pred.load_state_dict(torch.load(cond_m, map_location = torch.device('cpu')))
    #print("Loading from %s" % agg_e)
    model.agg_embed_layer.load_state_dict(torch.load(agg_e))
    #print("Loading from %s" % sel_e)
    model.sel_embed_layer.load_state_dict(torch.load(sel_e))
    #print("Loading from %s" % cond_e)
    model.cond_embed_layer.load_state_dict(torch.load(cond_e))

    return model

def modelWithoutTrainEmb(model):
    
    agg_m, sel_m, cond_m = best_model_name(args)
    #print("Loading from %s" % agg_m)
    model.agg_pred.load_state_dict(torch.load(agg_m, map_location = torch.device('cpu')))
    #print("Loading from %s" % sel_m)
    model.sel_pred.load_state_dict(torch.load(sel_m, map_location = torch.device('cpu')))    
    #print("Loading from %s" % cond_m)
    model.cond_pred.load_state_dict(torch.load(cond_m, map_location = torch.device('cpu')))    
    
    return model
    
        
def builddfFromwiki(td : dict):
    cols = td["header"]
    df = pd.DataFrame(columns=cols)
    for i,col in enumerate(cols):
        df[col] = [str(zeile[i]) for zeile in td["rows"]]
    return df

def loadtables():
    tables =OrderedDict()
    tablesLocation = getAbsolutePath("NLtoSQLConverter/data/test_tok.tables.jsonl")

    with open(tablesLocation) as f:
        content = f.readlines()[0:20]
    for zeile in content:
        zeile = zeile.strip()
        zd = json.loads(zeile)
        zd["df"] = builddfFromwiki(zd)
        tables[zd["id"]] = zd

    return tables

def load_all_data():
    model = createModel();
    tables = loadtables()
    
    if train_emb:
        model = modelWithTrainEmb(model)
    else:
        model = modelWithoutTrainEmb(model)
        
    return model,tables
    
def getAbsolutePath(path):
    missingForAbsolutePath = os.path.join(os.path.abspath(".."),"NLtoSQLBot")
    temp = path.split("/")
    for i in temp:
        missingForAbsolutePath = os.path.join(missingForAbsolutePath,i)
    return missingForAbsolutePath