.. NQLtoSQL Bot.A documentation master file, created by
   sphinx-quickstart on Sun Mar  1 22:17:56 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to NQLtoSQL Bot.A's documentation!
==========================================

.. toctree::
   :maxdepth: 2

   installation.rst
   quickstart.rst
   code.rst

Welcome to our project documentation.