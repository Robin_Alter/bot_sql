Installation
==========================================

First of all you need a Windows 10 64bit computer with python 3.6 and a cuda enabled graphic card after that you can install our requirements with:

    ``pip install -r requirements.txt``

After that you will need to download a few files and place them to specific locations:

1. Download `data.tar.bz2`. From the SQLNet Github Page https://github.com/xiaojunxu/SQLNet/blob/master/data.tar.bz2 and unpack it. After that, you should have the folders 'data' and 'data_resplit'. It should be placed in the NLtoSQLConverter folder.
2. Download the 42B.300d Glove embedding from [here](https://github.com/stanfordnlp/GloVe) or directly from http://nlp.stanford.edu/data/wordvecs/glove.42B.300d.zip. Unzip the 'glove.42B.300d.txt' file and put it into the folder 'NLtoSQLConverter/glove'

Perfect. Now you have completed all steps necessary to get our software running.