Quickstart
=========================

All right, the software is installed and you want to start using it now? 

We offer 2 modes of operation for our chatbot, you can use it with our VS Code plugin or you can use our CLI program.