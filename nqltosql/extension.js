const vscode = require('vscode');
const request = require('request');

let selectedDatabase = "";
let statusBarItem = vscode.window.createStatusBarItem(vscode.StatusBarAlignment.Right, 1000);

async function getUserInput(test) {
	if(selectedDatabase == "") {
		const database = await vscode.window.showInputBox({ prompt: 'Please input Database.' });
		selectedDatabase = database;
		statusBarItem.text = "DB: " + database;
		statusBarItem.show();
	} else {
		const nql = await vscode.window.showInputBox({ prompt: 'Please input a NL Statement.' });
		let sql = "";
		request.post(
			{
			  url: 'http://localhost:8000',
			  body: nql
			},
			function (err, httpResponse, body) {
			  sql = body;
			  test.edit((editor) => {
				editor.insert(vscode.window.activeTextEditor.selection.active,sql);
			  })
			}
		);
	}
}

/**
 * @param {vscode.ExtensionContext} context
 */
function activate(context) {
	let disposable = vscode.commands.registerCommand('extension.nqltoSQL', function () {
		getUserInput(vscode.window.activeTextEditor);
	});
	
	context.subscriptions.push(disposable);
    context.subscriptions.push(statusBarItem);
}
exports.activate = activate;

function deactivate() {}

module.exports = {
	activate,
	deactivate
}